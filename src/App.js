import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

// this is where you can put CSS style for html elements
const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

// this is the main entry point for the web app
class App extends Component {
  constructor(props) {
    super(props);

    // this creates an inital state
    this.state = { values: [] };
  }

  // this is a method used to hit the server api
  getValuesButtonPressed = () => {
    var url = 'http://localhost:5555/api/values?id=5';

    // you hit the url using the same method as what the server uses e.g. POST, GET
    // you then receive the response back from the server
    // you then turn that into JSON to put on the state
    // catching any errors that may occur
    fetch(url, {
      method: 'GET',
      mode: 'cors',
      credentials: "same-origin",
      headers: {
        'Accept': 'application/json',
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
    .then(res => res.json())
    .then(response => {
      this.setState( {values : response} ) // update state using setState
    })
    .catch(error => console.error('Error:', error));
  }

  // this is what is called that creates all the html for the page
  render() {
    // this allows you to access the CSS from above
    const { classes } = this.props;

    return (
      <div>
        {/* This is a material ui component - https://material-ui.com/api/button/ - use this for all your html */}
        <Button variant="contained" color="primary" className={classes.button} onClick={this.getValuesButtonPressed}>
          Get Values
        </Button>

        {/* This will run over all the values that are stored in your state */}
        <p>Values</p>
        <ul>
          {this.state.values.map(function(value, index){
            return <li key={ index }>{value}</li>;
          })}
        </ul>
      </div>
    );
  }
}

export default withStyles(styles)(App);