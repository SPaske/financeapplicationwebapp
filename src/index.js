import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

// this will bootstrap the react app into the index.html file
ReactDOM.render(<App />, document.getElementById('root'));